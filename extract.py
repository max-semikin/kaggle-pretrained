import io
import bson
from skimage.data import imread
from skimage.io import imsave
import multiprocessing as mp
import os
from tqdm import tqdm


NCORE = 8

prod_to_category = mp.Manager().dict()  # note the difference

path_to_bson = '/public/Cdiscount/test.bson'
path_to_imgs = '/output'

def process(q, iolock):
    print('process')
    while True:
        d = q.get()
        if d is None:
            break
        product_id = d['_id']
        for e, pic in enumerate(d['imgs']):
            picture = imread(io.BytesIO(pic['picture']))
            p = os.path.join(path_to_imgs, str(product_id) + '-' + str(e) + '.jpg')
            print(p)
            imsave(p, picture)

            # do something with the picture, etc

def run():
    print(os.path.exists(path_to_bson))
    q = mp.Queue(maxsize=NCORE)
    iolock = mp.Lock()
    pool = mp.Pool(NCORE, initializer=process, initargs=(q, iolock))

    # process the file

    print('adding files to queue')
    data = bson.decode_file_iter(open(path_to_bson, 'rb'))
    for c, d in tqdm(enumerate(data)):
        if c % 10000 == 0:
            print(c)
        q.put(d)  # blocks until q below its max size

    print('added files to queue')

    # tell workers we're done

    for _ in range(NCORE):
        q.put(None)
    pool.close()
    pool.join()
